# Generated by Django 3.0.5 on 2020-04-23 03:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0002_delete_message2'),
    ]

    operations = [
        migrations.AlterField(
            model_name='karta',
            name='omonatDaftar',
            field=models.FileField(blank=True, null=True, upload_to='media', verbose_name='Карта расм'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='name',
            field=models.TextField(blank=True, null=True, verbose_name='Имя пользователя'),
        ),
    ]
