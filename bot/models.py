from django.utils import timezone
from django.db import models

# Create your models here.
class Profile(models.Model):
    external_id=models.PositiveIntegerField(
        verbose_name='ID пользователя',
        unique=True
    )
    name = models.TextField('Имя пользователя',  null=True, blank=True)

    def __str__(self):
        return f'#{self.external_id}{self.name}'

    class Meta:
        verbose_name='Профиль'
        verbose_name_plural='Профили'

class Message(models.Model):
    profile = models.ForeignKey(
        to='bot.Profile',
        verbose_name='Профиль',
        on_delete=models.PROTECT,
        editable=True
    )
    name = models.TextField(
        verbose_name='Имя',
        editable=True

    )
    surname = models.TextField(
        verbose_name='Фамилия',
        editable=True

    )
    middlename = models.TextField(
        verbose_name='Отчество',
        editable=True

    )
    city = models.TextField(
     verbose_name = 'Қорақалпоғистон Республикаси, вилоят ва шаҳар номи',
    )
    filial = models.TextField(
        verbose_name='Филиал номи (Банк хизматлар маркази номи)',
        editable=True
    )
    schet = models.TextField(max_length=20,
        verbose_name='Мижознинг 20 талик ҳисоб рақами',
                             editable=True

    )
    description = models.TextField(
        verbose_name='Чиқим  мақсади (тўлиқ маълумот пластик карточка номери ёки ҳисоб рақами ва ҳ.к)',
    )
    passport=models.FileField(blank=True, null=True, upload_to='media',

        verbose_name='passport',
                              editable=True

    )
    omonatDaftar=models.FileField(blank=True, null=True, upload_to='media',
        verbose_name='omonat_daftarcha'
    )
    created_at = models.DateTimeField(
        verbose_name='Время получения',
        editable=True,
        default=timezone.now
    )

    def __str__(self):
        return f'Сообщения {self.pk} от {self.profile}'

    class Meta:
        verbose_name='Ариза 1'
        verbose_name_plural='Ариза 1'





class Karta(models.Model):
    profile = models.ForeignKey(
        to='bot.Profile',
        verbose_name='Профиль',
        on_delete=models.PROTECT,
        editable=True
    )

    choice =  models.TextField(
     verbose_name = 'Банк пластик карточкалари бўйича онлайн ариза',

    )
    name = models.TextField(
        verbose_name='Имя',
        editable=True

    )
    surname = models.TextField(
        verbose_name='Фамилия',
        editable=True

    )
    middlename = models.TextField(
        verbose_name='Отчество',
        editable=True

    )
    city = models.TextField(
     verbose_name = 'Қорақалпоғистон Республикаси, вилоят ва шаҳар номи',
    )
    filial = models.TextField(
        verbose_name='Филиал номи (Банк хизматлар маркази номи)',
        editable=True
    )
    schet = models.TextField(max_length=20,
        verbose_name='Мижознинг карта рақами',
                             editable=True

    )

    passport = models.FileField(blank=True, null=True, upload_to='media',

                                verbose_name='passport',
                                editable=True

                                )

    omonatDaftar=models.FileField(blank=True, null=True, upload_to='media',
        verbose_name='Карта расм'
    )
    created_at = models.DateTimeField(
        verbose_name='Время получения',
        editable=True,
        default=timezone.now
    )
    phone = models.TextField(max_length=20,
                             verbose_name='Мижознинг телефон рақами',
                             editable=True

                             )
    def __str__(self):
        return f'Сообщения {self.pk} от {self.profile}'

    class Meta:
        verbose_name='Ариза Карта'
        verbose_name_plural='Ариза Карта'