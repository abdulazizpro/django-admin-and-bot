from django import forms
from .models import Profile, Message, Karta


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = (
            'external_id',
            'name',
        )
        widgets = {
            'name':forms.TextInput,
        }


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = (
            'name','surname','middlename', 'city','filial', 'schet', 'description','passport', 'omonatDaftar',
        )
        widgets = {
            'name':forms.TextInput,
        }



class KartaForm(forms.ModelForm):
    class Meta:
        model = Karta
        fields = (
            'choice', 'name','surname','middlename',  'city','filial', 'schet','passport', 'omonatDaftar','phone'
        )
        widgets = {
            'name':forms.TextInput,
        }
