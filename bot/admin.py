from django.contrib import admin

# Register your models here.
from .models import Profile, Message, Karta
from .forms import ProfileForm

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'external_id', 'name')
    form = ProfileForm

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'profile', 'name','surname','middlename', 'city','filial', 'schet', 'description','passport', 'omonatDaftar', 'created_at')


@admin.register(Karta)
class KartaAdmin(admin.ModelAdmin):
    list_display = ('id', 'profile', 'choice','name','surname','middlename', 'city','filial', 'schet','passport', 'omonatDaftar', 'created_at', 'phone')





