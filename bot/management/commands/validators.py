from typing import Optional


GENDER_MAP = {
    'Тошкент шахар': 'Тошкент шахар',
    'Тошкент вилоят': 'Тошкент вилоят',
    'Қорақолпогистон': 'Қорақолпогистон',

}
GENDER_MAP2 = {
    'Бухоро': 'Бухоро',
    'Хоразм': 'Хоразм',
    'Джиззах': 'Джиззах',
    'Қашқадарё': 'Қашқадарё',
}
GENDER_MAP3 = {
    'Навои': 'Навои',
    'Самарқанд': 'Самарқанд',
    'Сирдарьё': 'Сирдарьё',
    'Сурхандарьё': 'Сурхандарьё',
}
GENDER_MAP4 = {
    'Наманган': 'Наманган',
    'Андижон': 'Андижон',
    'Фаргона': 'Фаргона',
}


KARTA_MAP = {
    'Муддати тугаган пластик': ' Ариза 1',
    'Пластик картани блокдан ечиш': 'Ариза 2',
    'Янги карта очиш': 'Ариза3',
}


NAMVIL1_MAP = {
'Наманган Поп':'Наманган Поп',
'Наманган Чуст':'Наманган Чуст',
'Наманган Чортоқ':'Наманган Чортоқ',

}

NAMVIL2_MAP = {
'Наманган Амалиёт':'Наманган Амалиёт',
'Наманган Тошбулоқ':'Наманган Тошбулоқ',


}
NAMVIL3_MAP = {
'Наманган Жумашуй':'Наманган Жумашуй',
'Наманган Косонсой':'Наманган Косонсой'
}






#def gender_hru(gender: str) -> Optional[str]:
 #   return GENDER_MAP


def validate_age(text: str) -> Optional[int]:
    try:
        age = int(text)
    except (TypeError, ValueError):
        return None


    if age < 10000000000000000000 or age > 99999999999999999999:
        return None
    return age

def validate_age2(text: str) -> Optional[int]:
    try:
        age2 = int(text)
    except (TypeError, ValueError):
        return None


    if age2 < 1000000000000000 or age2 > 9999999999999999:
        return None
    return age2