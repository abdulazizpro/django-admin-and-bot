from telegram import KeyboardButton, InlineKeyboardButton, InlineKeyboardMarkup
from telegram import ReplyKeyboardMarkup

from bot.management.commands.bot import TITLES, CALLBACK_BUTTON1_LEFT, CALLBACK_BUTTON2_RIGHT, CALLBACK_BUTTON3_MORE

BUTTON1_HELP = "Omonat"
BUTTON2_TIME = "Karta"
BUTTON3_TIME = "Admin"


def get_base_reply_keyboard():
    keyboard = [
        [
            KeyboardButton(BUTTON1_HELP),
            KeyboardButton(BUTTON2_TIME),

        ],
        [KeyboardButton(BUTTON3_TIME),]
    ]
    return ReplyKeyboardMarkup(
        keyboard=keyboard,
        resize_keyboard=True,
    )


BUTTON1 = "Omonat1"
BUTTON2 = "Omonat2"
BUTTON3 = "Ortga"


def get_nex_reply_keyboard():
    keyboard = [
        [
            KeyboardButton(BUTTON1),
            KeyboardButton(BUTTON2),

        ],
        [KeyboardButton(BUTTON3),]
    ]
    return ReplyKeyboardMarkup(
        keyboard=keyboard,
        resize_keyboard=True,
    )


