import datetime
import os
import urllib
import requests
from django.core.management.base import BaseCommand
from django.conf import settings
from telegram import Bot, ParseMode
from telegram import Update
from telegram import InlineKeyboardButton
from telegram import InlineKeyboardMarkup
from telegram.ext import CallbackContext
from telegram.ext import CommandHandler, ConversationHandler,CallbackQueryHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater
from telegram.utils.request import Request
from bot.management.commands.validators import *
from bot.models import Message, Karta
from bot.models import Profile
from logging import getLogger
import telebot

def log_errors(f):

    def inner(*args, **kwargs):
        try:
            logger.info(f"Обращения в функцию {f.__name__}")
            return f(*args, **kwargs)
        except Exception as e:
            error_message = f'Произошла ошибка: {e}'
            print(error_message)
            raise e

    return inner

@log_errors
def do_start_key(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    text = update.message.text

    p, _ = Profile.objects.get_or_create(
         external_id=chat_id,
         defaults={
             'name': update.message.from_user.first_name,
      }
     )

    reply_text = f'''Ассалому алейкум! "Микрокредитбанк" акциядорлик тижорат банкининг алоқа ботига хуш келибсиз.
Бугунги кунда юртимизда коронавирус пандемияси тарқалишини олдини олиш мақсадида хамда банкка келиб кетишда турли хил чекловларни инобатга олиб, банк томонидан мижозларга қулайликлар яратиш мақсадида банк карталари ва ахоли омонатлари бўйича айирим хизмат турларини масофадан амалга ошириш учун онлайн ариза кабул килинади.'''
    update.message.reply_text(
        text=reply_text,
        reply_markup=get_base_inline_keyboard(),
    )

logger = getLogger(__name__)

NAME, SURNAME, MIDDLENAME, GENDER, AGE, FILIAL, DESCRIPTION, PASSPORT, OMONAT  = range(9)

NAME2,SURNAME2, MIDDLENAME2,KARTA, GENDER2, AGE2, FILIAL2, PHONE1, PASSPORT2, OMONAT2  = range(10)

######################################################
CALLBACK_BUTTON1_LEFT = "callback_button1_left"
CALLBACK_BUTTON2_RIGHT = "callback_button2_right"
CALLBACK_BUTTON3_MORE = "callback_button3_more"
CALLBACK_BUTTON1 = "callback_button1"
CALLBACK_BUTTON2 = "callback_button2"
CALLBACK_BUTTON3 = "callback_button3"

ToshkentViloyat="ToshkentViloyat"
ToshkentShahar="ToshkentShahar"
NamanganViloyat="NamanganViloyat"
AndijonViloyat="AndijonViloyat"
FargonaViloyat="FargonaViloyat"
BuxoroViloyat="BuxoroViloyat"
SamarqandViloyat="SamarqandViloyat"
XorazmViloyat="XorazmViloyat"
QashqadaryoViloyat="QashqadaryoViloyat"
SurxandaryoViloyat="SurxandaryoViloyat"
SirdaryoViloyat="SirdaryoViloyat"
NavoiViloyat="NavoiViloyat"
DjizaxViloyat="DjizaxViloyat"
Qoraqalpoq="Qoraqalpoq"

TITLES1={
ToshkentViloyat: "Тошкент вилоят",
ToshkentShahar: "Тошкент вилоят",
NamanganViloyat: "Тошкент вилоят",
AndijonViloyat: "Тошкент вилоят",
FargonaViloyat: "Тошкент вилоят",
BuxoroViloyat: "Тошкент вилоят",
SamarqandViloyat: "Тошкент вилоят",
XorazmViloyat: "Тошкент вилоят",
QashqadaryoViloyat: "Тошкент вилоят",
SurxandaryoViloyat: "Тошкент вилоят",
SirdaryoViloyat: "Тошкент вилоят",
NavoiViloyat: "Тошкент вилоят",
DjizaxViloyat: "Тошкент вилоят",
Qoraqalpoq: "Тошкент вилоят",


}

TITLES = {
    CALLBACK_BUTTON1_LEFT: "🏦Омонат",
    CALLBACK_BUTTON2_RIGHT: "💳Карта ️",
    CALLBACK_BUTTON3_MORE: "📞Оператор",
    CALLBACK_BUTTON1: "Ариза",
    CALLBACK_BUTTON3: "Ортга",

}



def get_base_inline_keyboard():
    """ Получить клавиатуру для сообщения
        Эта клавиатура будет видна под каждым сообщением, где её прикрепили
    """
    # Каждый список внутри `keyboard` -- это один горизонтальный ряд кнопок
    keyboard = [
        # Каждый элемент внутри списка -- это один вертикальный столбец.
        # Сколько кнопок -- столько столбцов
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON1_LEFT], callback_data=CALLBACK_BUTTON1_LEFT),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON2_RIGHT], callback_data=CALLBACK_BUTTON2_RIGHT),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON3_MORE], callback_data=CALLBACK_BUTTON3_MORE),
        ],

    ]
    return InlineKeyboardMarkup(keyboard)
def get_keyboard2():
    """ Получить вторую страницу клавиатуры для сообщений
        Возможно получить только при нажатии кнопки на первой клавиатуре
    """
    keyboard = [

        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON1], callback_data=CALLBACK_BUTTON1),


        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON3], callback_data=CALLBACK_BUTTON3),
        ],
    ]
    return InlineKeyboardMarkup(keyboard)
def get_keyboard3():
    """ Получить вторую страницу клавиатуры для сообщений
        Возможно получить только при нажатии кнопки на первой клавиатуре
    """
    keyboard = [

            [InlineKeyboardButton(TITLES1[NamanganViloyat], callback_data=NamanganViloyat),
             InlineKeyboardButton(TITLES1[FargonaViloyat], callback_data=FargonaViloyat),
             InlineKeyboardButton(TITLES1[AndijonViloyat], callback_data=AndijonViloyat)],
            [InlineKeyboardButton(TITLES1[BuxoroViloyat], callback_data=BuxoroViloyat),
             InlineKeyboardButton(TITLES1[XorazmViloyat], callback_data=XorazmViloyat),
             InlineKeyboardButton(TITLES1[DjizaxViloyat], callback_data=DjizaxViloyat),
             InlineKeyboardButton(TITLES1[QashqadaryoViloyat], callback_data=QashqadaryoViloyat)],
            [InlineKeyboardButton(TITLES1[NavoiViloyat], callback_data=NavoiViloyat),
             InlineKeyboardButton(TITLES1[SamarqandViloyat], callback_data=SamarqandViloyat),
             InlineKeyboardButton(TITLES1[SirdaryoViloyat], callback_data=SirdaryoViloyat),
             InlineKeyboardButton(TITLES1[SurxandaryoViloyat], callback_data=SurxandaryoViloyat) ],
            [InlineKeyboardButton(TITLES1[ToshkentViloyat], callback_data=ToshkentViloyat),
             InlineKeyboardButton(TITLES1[ToshkentShahar], callback_data=ToshkentShahar),
             InlineKeyboardButton(TITLES1[Qoraqalpoq], callback_data=Qoraqalpoq)
             ]
        ]
    return InlineKeyboardMarkup(keyboard)

def get_keyboardNamangan():
    """ Получить вторую страницу клавиатуры для сообщений
        Возможно получить только при нажатии кнопки на первой клавиатуре
    """
    keyboard = [

            [InlineKeyboardButton(TITLES1[NamanganViloyat], callback_data=NamanganViloyat),
             InlineKeyboardButton(TITLES1[FargonaViloyat], callback_data=FargonaViloyat),
             InlineKeyboardButton(TITLES1[AndijonViloyat], callback_data=AndijonViloyat)],
            [InlineKeyboardButton(TITLES1[BuxoroViloyat], callback_data=BuxoroViloyat),
             InlineKeyboardButton(TITLES1[XorazmViloyat], callback_data=XorazmViloyat),
             InlineKeyboardButton(TITLES1[DjizaxViloyat], callback_data=DjizaxViloyat),
             InlineKeyboardButton(TITLES1[QashqadaryoViloyat], callback_data=QashqadaryoViloyat)
             ]

        ]
    return InlineKeyboardMarkup(keyboard)
def keyboard_callback_handler(update: Update, context: CallbackContext):
    """ Обработчик ВСЕХ кнопок со ВСЕХ клавиатур
    """
    query = update.callback_query
    data = query.data
    now = datetime.datetime.now()

    # Обратите внимание: используется `effective_message`
    chat_id = update.effective_message.chat_id
    current_text = update.effective_message.text
    if data == CALLBACK_BUTTON1_LEFT:
        query.edit_message_text(
            text='Ариза -Ҳисобланган  ойлик фоиз омонат пул маблағларини банк нақдсиз шаклда чиқим қилиш бўйича онлайн ариза\n\n',

            parse_mode=ParseMode.MARKDOWN,
            reply_markup=get_keyboard2()
        )

    elif data == CALLBACK_BUTTON3:
        query.edit_message_text(
            text=f'Ариза турини танланг',
            reply_markup=get_base_inline_keyboard(),
        )
    elif data==CALLBACK_BUTTON1:

        query.edit_message_text(
            text="""/application1 команда устига босинг""",


        )


    elif data == CALLBACK_BUTTON2_RIGHT:
        query.edit_message_text(
            text="""/card команда устига босинг""",
        )
######################################################
def do_count(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id

    p, _ = Profile.objects.get_or_create(
         external_id=chat_id,
         defaults={
             'name': update.message.from_user.first_name,
         }
     )
    count = Message.objects.filter(profile=p).count()


    update.message.reply_text(
        text=f'У вас {count} сообщений',
    )
###########################################
def start_handler(update: Update, context: CallbackContext):
    # Спросить имя
    update.message.reply_text(
        'Исмингизни ёзинг:',
    )

    return NAME
def name_handler(update: Update, context: CallbackContext):
    # Получить имя
    context.user_data[NAME] = update.message.text
    logger.info('user_data: %s', context.user_data)

    update.message.reply_text(
        'Фамилиянгизни ёзинг:',
    )

    return SURNAME
def surname_handler(update: Update, context: CallbackContext):
    # Получить имя
    context.user_data[SURNAME] = update.message.text
    logger.info('user_data: %s', context.user_data)

    update.message.reply_text(
        'Шарифингизни ёзинг:',
    )

    return MIDDLENAME
def middname_handler(update: Update, context: CallbackContext):
    # Получить имя
    context.user_data[MIDDLENAME] = update.message.text
    logger.info('user_data: %s', context.user_data)

    # Спросить пол
    inline_buttons = InlineKeyboardMarkup(
        inline_keyboard=[
            [InlineKeyboardButton(text=value, callback_data=key) for key, value in GENDER_MAP4.items()],
            [InlineKeyboardButton(text=value, callback_data=key) for key, value in GENDER_MAP2.items()],
            [InlineKeyboardButton(text=value, callback_data=key) for key, value in GENDER_MAP3.items()],
            [InlineKeyboardButton(text=value, callback_data=key) for key, value in GENDER_MAP.items()],
        ],
    )
    update.message.reply_text(
        text='Вилоят/Шахарни танланг',
        reply_markup=inline_buttons,
    )
    return GENDER
def filial_handler(update: Update, context: CallbackContext):
    # Получить пол
    gender = update.callback_query.data
    gender = str(gender)
    # if gender not in (GENDER_MAP, GENDER_MAP2, GENDER_MAP3, GENDER_MAP4):
    # Этой ситуации не должно быть для пользователя! То есть какое-то значение
    # в кнопках есть, но оно не включено в список гендеров
    #   update.effective_message.reply_text('Что-то пошло не так, обратитесь к администратору бота')
    #  return GENDER

    context.user_data[GENDER] = gender
    logger.info('user_data: %s', context.user_data)

    # Спросить возраст


    update.effective_message.reply_text(
        text='Филиал номи ёки МФОни ёзинг:',

    )

    return FILIAL
def age_handler(update: Update, context: CallbackContext):
    # Получить пол

    context.user_data[FILIAL] = update.message.text
    logger.info('user_data: %s', context.user_data)

    #if gender not in (GENDER_MAP, GENDER_MAP2, GENDER_MAP3, GENDER_MAP4):
        # Этой ситуации не должно быть для пользователя! То есть какое-то значение
        # в кнопках есть, но оно не включено в список гендеров
     #   update.effective_message.reply_text('Что-то пошло не так, обратитесь к администратору бота')
      #  return GENDER



    # Спросить возраст
    update.effective_message.reply_text(
        text='20 талик хисоб рақамни ёзинг:',
    )
    return AGE
def prefinish_handler(update: Update, context: CallbackContext):
    # Получить возраст
    age = validate_age(text=update.message.text)
    if age is None:
        update.message.reply_text('Илтимос, тўгри рақам киритинг!')
        return AGE

    context.user_data[AGE] = age
    logger.info('user_data: %s', context.user_data)

    # Спросить причину
    update.effective_message.reply_text(
        text='Чиқим  мақсади (тўлиқ маълумот пластик карточка номери ёки ҳисоб рақами :',
    )
    return DESCRIPTION
def prefinish2_handler(update: Update, context: CallbackContext):
    context.user_data[DESCRIPTION] = update.message.text
    logger.info('user_data: %s', context.user_data)

    # Спросить причину
    update.effective_message.reply_text(
        text='Паспортнинг асл нусхаси (турар жойи билан биргаликда):',
    )
    return PASSPORT
def prefinish3_handler(update: Update, context: CallbackContext):
    context.user_data[PASSPORT] = update.message.photo[-1].file_id
    logger.info('user_data: %s', context.user_data)



    # Спросить причину
    update.effective_message.reply_text(
        text='Омонат дафтарчаси асл нусхаси (мижознинг маълумолар тўғрисидаги ва кирим-чиқим варақлари):',
    )
    return OMONAT

    # TODO: вот тут запись в базу финала

    # TODO 2: очистить `user_data`
def finish_handler(update: Update, context: CallbackContext):
    context.user_data[OMONAT] = update.message.photo[-1].file_id
    logger.info('user_data: %s', context.user_data)
    # Завершить диалог
    update.message.reply_text(f'''
Барча малумотлар сақланди! /start босинг асосий экранга қайтиш учун \n\n
Сиз: {context.user_data[NAME]} {context.user_data[SURNAME]} {context.user_data[MIDDLENAME]},\nШахар: {(context.user_data[GENDER])},\nФилиал номи ёки МФО: {context.user_data[FILIAL]}\nСчёт: {context.user_data[AGE]}\nЧиқим  мақсади: {context.user_data[DESCRIPTION]}  
''')
    chat_id = update.message.chat_id
    p, _ = Profile.objects.get_or_create(
        external_id=chat_id,
        defaults={
            'name': update.message.from_user.first_name,
        }
    )
    m = Message(
        profile=p,
        name=context.user_data[NAME],
        surname=context.user_data[SURNAME],
        middlename=context.user_data[MIDDLENAME],
        city=context.user_data[GENDER],
        filial=context.user_data[FILIAL],
        schet=context.user_data[AGE],
        description=context.user_data[DESCRIPTION],
        passport=context.user_data[PASSPORT],
        omonatDaftar=context.user_data[OMONAT]
    )
    m.save()
def cancel_handler(update: Update, context: CallbackContext):
    """ Отменить весь процесс диалога. Данные будут утеряны
    """
    update.message.reply_text('Отмена. Для начала с нуля нажмите /start')
    return ConversationHandler.END
def echo_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        'Нажмите /start для заполнения анкеты!',
    )
###########################################
def do_start(update: Update, context: CallbackContext):
    update.message.reply_text(

        text="Привет! Отправь мне свои данные",
    )
conv_handler = ConversationHandler(
        entry_points=[
            CommandHandler('application1', start_handler),
        ],
        states={
            NAME: [
                MessageHandler(Filters.text, name_handler, pass_user_data=True),
            ],
            SURNAME: [
                MessageHandler(Filters.text, surname_handler, pass_user_data=True),
            ],
            MIDDLENAME: [
                MessageHandler(Filters.text, middname_handler, pass_user_data=True),
            ],
            GENDER: [
                CallbackQueryHandler(filial_handler, pass_user_data=True),
            ],
            FILIAL: [
                MessageHandler(Filters.text, age_handler, pass_user_data=True),
            ],
            AGE: [
                MessageHandler(Filters.text, prefinish_handler, pass_user_data=True),
            ],
            DESCRIPTION: [
                MessageHandler(Filters.all, prefinish2_handler, pass_user_data=True),
            ],
            PASSPORT: [
                MessageHandler(Filters.all, prefinish3_handler, pass_user_data=True),
            ],
            OMONAT: [
                MessageHandler(Filters.all, finish_handler, pass_user_data=True),
            ],



        },
        fallbacks=[
            CommandHandler('start', do_start_key),
        ],
    )
########################################################################################################################

########################################################################################################################
########################################################################################################################
def start_handler_karta1(update: Update, context: CallbackContext):
    # Спросить имя
    update.message.reply_text(
        'Исмингизни ёзинг:',
    )

    return NAME2
def name_handler2_karta(update: Update, context: CallbackContext):
    # Получить имя
    context.user_data[NAME2] = update.message.text
    logger.info('user_data: %s', context.user_data)

    update.message.reply_text(
        'Фамилиянгизни ёзинг:',
    )

    return SURNAME2
def surname_handler2_karta(update: Update, context: CallbackContext):
    # Получить имя
    context.user_data[SURNAME2] = update.message.text
    logger.info('user_data: %s', context.user_data)

    update.message.reply_text(
        'Шарифингизни ёзинг::',
    )

    return MIDDLENAME2
def middname_handler2_karta(update: Update, context: CallbackContext):
    # Получить имя
    context.user_data[MIDDLENAME2] = update.message.text
    logger.info('user_data: %s', context.user_data)

    inline_buttons = InlineKeyboardMarkup(
        inline_keyboard=[
            [InlineKeyboardButton(text=value, callback_data=key) for key, value in KARTA_MAP.items()],
        ],
    )
    # Спросить имя
    update.message.reply_text(
        text='Ариза турини танланг:\n'
             'Ариза 1 - Муддати тугаган пластик карточкаларни янгисига алмаштириш бўйича онлайн ариза\n\n'
             'Ариза 2 - Блокланган пластик карточкаларни блокдан чиқариш бўйича онлайн ариза\n\n'
             'Ариза 3 - Янги пластик карта очиш\n\n',
        reply_markup=inline_buttons,

    )
    return KARTA
def name_handler_karta(update: Update, context: CallbackContext):
    karta = update.callback_query.data
    karta = str(karta)
    context.user_data[KARTA] = karta
    logger.info('user_data: %s', context.user_data)


    # Спросить пол
    inline_buttons = InlineKeyboardMarkup(
        inline_keyboard=[
            [InlineKeyboardButton(text=value, callback_data=key) for key, value in GENDER_MAP4.items()],
            [InlineKeyboardButton(text=value, callback_data=key) for key, value in GENDER_MAP2.items()],
            [InlineKeyboardButton(text=value, callback_data=key) for key, value in GENDER_MAP3.items()],
            [InlineKeyboardButton(text=value, callback_data=key) for key, value in GENDER_MAP.items()],
        ],
    )
    update.effective_message.reply_text(
        text='Вилоят/Шахарни танланг',
        reply_markup=inline_buttons,
    )
    return GENDER2
def filial_handler2_karta(update: Update, context: CallbackContext):
    # Получить пол
    gender = update.callback_query.data
    gender = str(gender)
    context.user_data[GENDER2] = gender
    logger.info('user_data: %s', context.user_data)

    # Спросить возраст
    update.effective_message.reply_text(
        text='Филиал номи ёки МФОсини ёзинг:',
    )
    return FILIAL2
def age_handler2_karta(update: Update, context: CallbackContext):
    # Получить пол
    context.user_data[FILIAL2] = update.message.text
    logger.info('user_data: %s', context.user_data)

    #if gender not in (GENDER_MAP, GENDER_MAP2, GENDER_MAP3, GENDER_MAP4):
        # Этой ситуации не должно быть для пользователя! То есть какое-то значение
        # в кнопках есть, но оно не включено в список гендеров
     #   update.effective_message.reply_text('Что-то пошло не так, обратитесь к администратору бота')
      #  return GENDER



    # Спросить возраст
    update.effective_message.reply_text(
        text='Банк пластик карточка рақам киритинг:',
    )
    return AGE2

def prefinish_handler2_karta(update: Update, context: CallbackContext):
    # Получить возраст
    age2 = validate_age2(text=update.message.text)
    if age2 is None:
        update.message.reply_text('Илтимос, тўгри рақам киритинг!')
        return AGE2

    context.user_data[AGE2] = age2
    logger.info('user_data: %s', context.user_data)

    # Спросить причину
    update.effective_message.reply_text(
        text='Паспортнинг асл нусхаси (турар жойи билан биргаликда):',
    )
    return PASSPORT2

def prefinish2_handler2_karta(update: Update, context: CallbackContext):

    context.user_data[PASSPORT2] = update.message.photo[-1].file_id
    logger.info('user_data: %s', context.user_data)




    # Спросить причину
    update.effective_message.reply_text(
        text='Банк пластик карточка расми (агарда йўқолган бўлcа унда изох ёзиб қолдиринг):',
    )
    return OMONAT2

def prefinish3_handler2_karta(update: Update, context: CallbackContext):
        context.user_data[OMONAT2] = update.message.text or update.message.photo[-1].file_id
        logger.info('user_data: %s', context.user_data)

        # Спросить причину
        update.effective_message.reply_text(
            text='Телефон рақамингиз:',
        )
        return PHONE1

    # TODO: вот тут запись в базу финала

    # TODO 2: очистить `user_data`
def finish_handler2_karta(update: Update, context: CallbackContext):
    context.user_data[PHONE1] = update.message.text
    logger.info('user_data: %s', context.user_data)
    # Завершить диалог
    update.message.reply_text(f'''
Барча малумотлар сақланди! /start босинг асосий экранга қайтиш учун \n\n
Сиз: {context.user_data[NAME2]} {context.user_data[SURNAME2]} {context.user_data[MIDDLENAME2]}, Ариза тури: {context.user_data[KARTA]} \nШахар: {(context.user_data[GENDER2])},\nФилиал номи ёки МФО: {context.user_data[FILIAL2]}
\nСчёт: {context.user_data[AGE2]}\n\nТелефон: {context.user_data[PHONE1]}
''')

    chat_id = update.message.chat_id

    p, _ = Profile.objects.get_or_create(
        external_id=chat_id,
        defaults={
            'name': update.message.from_user.username,
        }
    )
    m = Karta(
        profile=p,

        name=context.user_data[NAME2],
        surname=context.user_data[SURNAME2],
        middlename=context.user_data[MIDDLENAME2],
        city=context.user_data[GENDER2],
        filial=context.user_data[FILIAL2],
        schet=context.user_data[AGE2],
        choice=context.user_data[KARTA],
        passport=context.user_data[PASSPORT2],
        omonatDaftar=context.user_data[OMONAT2],
        phone=context.user_data[PHONE1]



    )
    m.save()




def cancel_handler3(update: Update, context: CallbackContext):
    """ Отменить весь процесс диалога. Данные будут утеряны
    """
    update.message.reply_text('Отмена. Для начала с нуля нажмите /start')
    return ConversationHandler.END



def echo_handler3(update: Update, context: CallbackContext):
    update.message.reply_text(
        'Нажмите /start для заполнения анкеты!',
    )
###########################################

def do_start3(update: Update, context: CallbackContext):
    update.message.reply_text(

        text="Привет! Отправь мне свои данные",
    )

conv_handler3 = ConversationHandler(
        entry_points=[
            CommandHandler('card', start_handler_karta1),
        ],

        states={
            NAME2: [
                MessageHandler(Filters.text, name_handler2_karta, pass_user_data=True),
            ],
            SURNAME2: [
                MessageHandler(Filters.text, surname_handler2_karta, pass_user_data=True),
            ],
            MIDDLENAME2: [
                MessageHandler(Filters.text, middname_handler2_karta, pass_user_data=True),
            ],
            KARTA: [
                CallbackQueryHandler(name_handler_karta, pass_user_data=True),
            ],
            GENDER2: [
                CallbackQueryHandler(filial_handler2_karta, pass_user_data=True),
            ],
            FILIAL2: [
                MessageHandler(Filters.text, age_handler2_karta, pass_user_data=True),
            ],
            AGE2: [
                MessageHandler(Filters.text, prefinish_handler2_karta, pass_user_data=True),
            ],

            PASSPORT2: [
                MessageHandler(Filters.all, prefinish2_handler2_karta, pass_user_data=True),
            ],
            OMONAT2: [
                MessageHandler(Filters.all, prefinish3_handler2_karta, pass_user_data=True),
            ],

            PHONE1: [
                MessageHandler(Filters.all, finish_handler2_karta, pass_user_data=True),
            ],



        },
        fallbacks=[
            CommandHandler('start', do_start_key),
        ],

    )
########################################################################################################################
########################################################################################################################


class Command(BaseCommand):
    help = 'Телеграм-бот'

    def handle(self, *args, **options):
        # 1 -- правильное подключение
        request = Request(
            connect_timeout=0.5,
            read_timeout=1.0,
        )
        bot = Bot(
            request=request,
            token=settings.TOKEN,

        )
        print(bot.get_me())

        # 2 -- обработчики
        updater = Updater(
            bot=bot,
            use_context=True,
        )
        message_handler2 = CommandHandler('start', do_start_key)
        updater.dispatcher.add_handler(message_handler2)
        updater.dispatcher.add_handler(conv_handler)
        updater.dispatcher.add_handler(conv_handler3)
        updater.dispatcher.add_handler(MessageHandler(Filters.command, echo_handler))
        buttons_handler = CallbackQueryHandler(callback=keyboard_callback_handler)
        updater.dispatcher.add_handler(buttons_handler)
        # 3 -- запустить бесконечную обработку входящих сообщений
        updater.start_polling()
        updater.idle()
